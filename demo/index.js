#!/usr/bin/env node

const M = {
  Scavader: require("../index")
};

// Example of scavader using the `./scaffold.example` folder
let scavader = new M.Scavader("./projects/web-application", "Scavader");
scavader.run("questions").then(result => {
  console.log(result);
});
