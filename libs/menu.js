const M = {
  figlet: require("figlet"),
  chalk: require("chalk"),
  clear: require("clear"),
  inquirer: require("inquirer")
};

/**
 * Displays a menu and asks user some questions *sequentially*.
 *
 * ### Example of usage
 *
 *  ```
 *  // Create the questions for the menu
 *  const questions = [
 *  {
 *    message: "Choose where to invade:",
 *    name: "initApplication",
 *    command: "initApplication",
 *    type: "checkbox",
 *    resolver: resolveAppType, // Just to demonstrate the how to use a resolver
 *    choices: [
 *      { name: "Frontend web", value: "frontend", checked: true },
 *      { name: "Backend API", value: "backend", checked: true }
 *    ],
 *    next: [
 *      {
 *        message: `What frontend folder do you want to use ?`,
 *        name: "frontendDirectory",
 *        if: "frontend",
 *        type: "input",
 *        default: "client/"
 *      }, …
 *    ],
 *    filterNext: (val, answerResult, resolverResult) => {
 *      return resolverResult.indexOf(val.if) > -1;
 *    },
 *  ]
 *
 *  // Launch the menu with the specified questions
 *  const menu = new Menu('Scavader')
 *  menu.run("questions").then( (result) => {
 *    console.log(result);
 *  }
 *  ```
 *
 *  ### Question object
 *
 *  Check the [Inquirer module](https://www.npmjs.com/package/inquirer) to see
 *  what field can be used. Note that the `command` and the `resolver` fields
 *  are exclusively used by the module provided by `Scavader`.
 *
 * @module menu
 * @param {string} Title - The title to display when running the menu
 */
class Menu {
  constructor(title) {
    this.title = title;
    this._initialized = false;
  }

  /**
   * Launch the menu of questions.
   *
   * @param {array} questions The list of questions to be asks.
   */
  run(questions) {
    return new Promise(done => {
      this._intro().then(() => {
        this._next(questions, [], done);
      });
    });
  }

  /** Display the title introduction only once */
  _intro() {
    M.clear();
    return new Promise(resolve => {
      if (this._initialized) {
        resolve();
      } else {
        M.figlet(this.title, (err, data) => {
          console.log(M.chalk.blue(data));
          resolve();
        });
      }
    });
  }

  /**
   * Run the next set of questions
   * @param {array} questions The questions list to be asked.
   * @param {function} finalDone The final resolver to be executed
   */
  async _next(questions, answers = [], finalDone = null) {
    for (const question of questions) {
      let answerResult = {
        question: question,
        value: null
      };
      let resolverResult = null;

      // Asks the question
      await M.inquirer.prompt(question).then(async result => {
        // Execute the resolver passing the resulting answer
        let resultValue = result[question.name];
        if (question.resolver) {
          answerResult.value = await question.resolver(resultValue);
        } else {
          answerResult.value = resultValue;
        }

        // Update the tree of answers
        answers.push(answerResult);
      });

      // Asks the next questions
      let nextQuestions = question.next;
      if (nextQuestions) {
        if (question.filterNext) {
          nextQuestions = nextQuestions.filter(val => {
            return question.filterNext(val, answerResult);
          });
        }
        await this._next(nextQuestions, answers);
      }
    }

    // Execute the final resolver of the promise to terminate
    if (finalDone) {
      finalDone(answers);
    }
  }
}

module.exports = Menu;
