// Very basic CLI
const M = {
  minimist: require("minimist"),
  chalk: require("chalk"),
  fs: require("fs-extra"),
  promisify: require("util").promisify,
  inquirer: require("inquirer")
};

module.exports = (Scavader, SCAFFOLD_DEFAULT_DIR) => {
  const argv = M.minimist(process.argv.slice(2));

  if (argv._.length === 0) {
    // Example of command line usage
    console.log(
      "\nCommand line usage examples:\n",
      M.chalk.gray("• Generate a scaffold directory to work on\n"),
      M.chalk.cyan("$ scavader init -d scaffold\n"),
      M.chalk.gray("• Scaffold from the generated scaffold\n"),
      M.chalk.cyan("$ scavader run -d scaffold\n"),
      M.chalk.gray("• To scaffold from built-in 'web-application' project\n"),
      M.chalk.cyan("$ scavader init -p web-application"),
    );

    // Help Menu
    console.log(
      "\nOPTIONS:\n",
      M.chalk.blue("init"),
      M.chalk.gray("\tCreates a new scavader structure.\n"),
      M.chalk.blue("run"),
      M.chalk.gray("\tRun a specific, previously created, scaffold.\n"),
      M.chalk.blue("-d directory"),
      M.chalk.gray("\t\tSpecify the directory to use (default: 'scaffold/')\n"),
      M.chalk.blue("-q questions files"),
      M.chalk.gray("\tSpecify the question file to use (default: 'questions')\n"),
      M.chalk.blue("-p [PROJECT]"),
      M.chalk.gray("\tSpecify the project folder scaffold from (default: 'basic')\n")
    )
    process.exit(0);
  }

  // Parse the directory option
  let directory = argv.d;
  if (!argv.d) {
    console.log(
      M.chalk.yellow(
        `Scaffold directory not specified, using '${SCAFFOLD_DEFAULT_DIR}'`
      )
    );
    directory = SCAFFOLD_DEFAULT_DIR;
  }

  // Get the project to scaffold
  let PROJECT = argv.p ? argv.p : "basic";
  let projectsDirList = M.fs.readdirSync(__dirname + "/../projects");
  if(projectsDirList.indexOf(PROJECT) == -1 && typeof argv.p !== "boolean"){
    console.log(M.chalk.yellow(`WARNING: selected project '${PROJECT}' does not exists`))
  }

  // Init a scaffold
  let INIT = () => {
    return new Promise(async finalResolve => {
      if (argv._.includes("init")) {
        console.log("Scavading:", M.chalk.yellow(directory));
        if (!M.fs.existsSync(directory)) {
          // Create the scaffold directory
          await M.fs.mkdir(directory);

          // Get the list of the projects that can be scaffold
          let projectsList = projectsDirList.map( (dir) => {
            return {name: dir.toUpperCase(), value: dir}
          });

          if(projectsDirList.indexOf(PROJECT) == -1) {
            // Ask which project to scaffold
            let projectChoice = await M.inquirer.prompt([{
              name: "projectScaffold",
              message: "What project to scaffold?",
              type: "list",
              choices: projectsList,
            }]);
            PROJECT = projectChoice.projectScaffold;
          }

          // Copy the _template.js questions file
          await M.fs.copy(
            `${__dirname}/../projects/${PROJECT}/`,
            `${directory}/`
          );

          finalResolve();
        } else {
          console.error(
            M.chalk.red(`Directory '${directory}' already exists, aborting...`)
          );
          process.exit(1);
        }
      } else {
        finalResolve();
      }
    });
  };

  // The questions file to use
  let questions = argv.q ? argv.q : "questions";

  // Run a scaffold
  let RUN = () => {
    if (argv._.includes("run")) {
      if (!M.fs.existsSync(directory)) {
        console.error(
          M.chalk.red(`SCAFFOLD DIRECTORY ${directory} does not exists`)
        );
      } else {
        console.log("Let's run ", M.chalk.yellow(directory));
        let scavader = new Scavader(`${directory}/`, "Scavader");
        scavader.run(questions).then(result => {
          console.log(result);
        });
      }
    }
  };

  // Launch the CLI
  INIT().then(
    setTimeout(() => {
      M.chalk.blue(`Running your scaffold (directory=${directory}`);
      RUN();
    }, 2000)
  );
};
