const EXT_MODULES = [
  "chalk",
  "inquirer",
  "clear",
  "figlet",
  "clui",
  "minimist"
];

// Checks if all modules are correctly installed
EXT_MODULES.forEach(module => {
  try {
    require(module);
  } catch (e) {
    throw new Error(
      `IMPORTS: module ${module} is not installed. Please run 'npm install'`
    );
  }
});
