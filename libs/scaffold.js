const M = {
  chalk: require("chalk"),
  inquirer: require("inquirer"),
  path: require("path"),
  fs: require("fs")
};

/**
 * The scaffolder that executes JS commands using a `scaffold` array and
 * scripts in a specified directory.
 *
 * ### Example of usage
 *
 * ```
 * let Scaffold = new Scaffold('commands/', scaffolds)
 * let results = Scaffold.run().then( (result) => {
 *  console.log(results);
 *  }
 * ```
 *
 * ### Scaffold Object
 *
 * ```
 * let scaffolds = [
 *   {command: "commandName", value: ["value1", "value2"]}, // <-- Scaffold object
 *   {command: "commandName2", value: "someValue"}          // <-- Scaffold object
 *   …
 * ]
 * ```
 *
 * Note: the `comandName.js` and `commandName2.js` should exists and contain an
 * exported function that will receive the `value`.  If the file is not found,
 * it will simply return the user input value. The returned value of the
 * scaffold script will be inscribe in the scaffold object in the `cmdResult`
 * field.
 *
 * Note: the `value` is what the user answered, the `cmdResult` is what the command
 * returned.
 *
 * @module scaffold
 * @param {string} dir The directory of the commands (default: "commands/")
 * @param {array} scaffolds The scaffold structure to be executed.
 */
class Scaffold {
  constructor(commandsDir = "commands/") {
    this.commandsDir = M.path.normalize(commandsDir);
  }

  /** Run the initialized scaffolds */
  run(scaffolds = []) {
    const results = {};
    return new Promise((resolve, reject) => {
      scaffolds.forEach(scaffold => {
        scaffold.cmdResult = this._executeCommand(
          scaffold.command,
          scaffold.value,
          results,
          reject
        );
        results[scaffold.name] = scaffold;
      });
      resolve(results);
    });
  }

  /**
   * Execute the corresponding command that stands in the specified commands
   * directory.
   *
   * @param {string} command The command to execute (the JS script without the extension).
   * @param {string} value The value to pass to the command that will be executed.
   * @param {object} scaffold The current scaffold structure (useful to check
   * and see if another command was executed or not).
   */
  _executeCommand(command, value, results, reject) {
    // Command path in the current running directory
    let commandPath =
      process.cwd() +
      "/" +
      M.path.normalize(`${this.commandsDir}/${command}.js`);

    // Later command script file variable
    let commandModule = undefined;

    // Check if the command file exists
    if (command !== undefined || M.fs.existsSync(commandPath)) {
      // Require the command script file.
      try {
        commandModule = require(commandPath);
      } catch (e) {
        console.log(
          M.chalk.red("ERROR:"),
          `${M.chalk.red(command)} ${commandPath} returned an error`
        );
        console.error(e);
        return reject(e); // Note: given by the `run` function
      }

      try {
        return commandModule(value, results, reject);
      } catch (e) {
        console.log(e);
        console.log(
          M.chalk.red("ERROR:"),
          `${M.chalk.red(command)} ${commandPath} does not exists in the ${
            this.commandsDir
          } directory`
        );
      }
    }

    // Just returning the value in case there is no command to execute
    return value;
  }
}

module.exports = Scaffold;
