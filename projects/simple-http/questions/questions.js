module.exports = [
  {
    message: `What folder name do you want to create ?`,
    name: "initSimpleHTTP",
    command: "initSimpleHTTP",
    type: "input",
    default: "./"
  },
];
