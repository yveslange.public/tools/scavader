const M = {
  fs: require("fs"),
  path: require("path"),
  exec: require("child_process").execSync
};


let DIRECTORY="./"

module.exports = (value, previousResult) => {
  DIRECTORY = M.path.resolve(process.cwd(), value);
  let exists = M.fs.existsSync(value);
  if (!exists) {
    M.fs.mkdirSync(value, { recursive: true });
    console.log(`${MODULE_NAME}> Directory ${value} created`);
  }
  process.chdir(DIRECTORY);

  // Install dev deps
  console.log("Init of the project and installing dependencies. Please wait...")
  M.exec("npm init -y", {cwd: DIRECTORY});
  M.exec("npm install --save-dev fs-extra http-server browser-sync", {cwd: DIRECTORY});
  M.exec(`echo "<!DOCTYPE HTML>\n<html>\n<head></head>\n<body>\n  <h1>Hello world!</h1>\n</body>\n</html>" > index.html`);

  // Update package.json start script
  M.fs = require("fs-extra");
  let packageJSON = M.fs.readJsonSync("package.json");
  packageJSON.scripts.start = "./node_modules/.bin/browser-sync start --server -f index.html"
  M.fs.writeJsonSync("package.json", packageJSON, {spaces: 2});

  console.log("Start the HTTP server:\n$ npm run start");
  return true
};
