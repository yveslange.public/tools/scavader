// Some libs
const M = {
  exec: require('child_process').execSync
};

// Install missing dependencies to run the script
const DEPS = ['chalk']
DEPS.forEach( (dep) => {
  try{
    require(dep)
  } catch(e) {
    console.log(`Installing dependency ${dep}...`)
    M.exec(`npm install ${dep}`);
  }
  M[dep] = require(dep);
});


let PKG_NAME = "scavader";
try {
  M.pkg = require(process.cwd() + "/packages.json");
  PKG_NAME = M.pkg.name;
} catch (e) {
  console.error(
    `${M.chalk.yellow("Warning")}: Could not find 'package.json' file`
  );
}

// Example of resolver
let resolveAppType = result => {
  return new Promise(resolve => {
    console.log(M.chalk.cyan("> You choose", result));
    resolve(result);
  });
};

module.exports = [
  {
    message: "Choose where to invade:",
    name: "initApplication",
    command: "initApplication",
    type: "checkbox",
    choices: [
      { name: "Frontend web", value: "frontend", checked: true },
      { name: "Backend API", value: "backend", checked: true }
    ],
    resolver: resolveAppType, // Just to demonstrate the how to use a resolver
    next: [
      {
        message: `What frontend folder do you want to use ?`,
        name: "createFrontendDirectory",
        command: "createDirectory",
        if: "frontend",
        type: "input",
        default: "client/"
      },
      {
        message: "Choose your frontend modules",
        name: "installFrontendModules",
        command: "installFrontendModules",
        if: "frontend",
        type: "checkbox",
        choices: [
          {
            name: "PWA LitElement",
            value: "pwa-litelement",
            checked: true
          },
          { name: "Firebase", value: "firebase", checked: true }
        ],
        next: [
          {
            message: `Choose the frontend firebase modules you want to install`,
            name: "installFrontendFirebaseModules",
            command: "installFrontendFirebaseModules",
            if: "firebase",
            type: "checkbox",
            choices: [
              {
                name: "Firebase - Firestore",
                value: "firebase-firestore",
                checked: true
              },
              {
                name: "Firebase - Authentication",
                value: "firebase-authentication",
                checked: true
              }
            ]
          }
        ],
        filterNext: (val, answerResult) => {
          return answerResult.value.indexOf(val.if) > -1;
        }
      },
      {
        message: `What backend folder do you want to use ?`,
        name: "createFrontendDirectory",
        command: "createDirectory",
        if: "backend",
        type: "input",
        default: "server/"
      },
      {
        message: "Choose your backend modules",
        name: "installBackendModules",
        command: "installBackendModules",
        if: "backend",
        type: "checkbox",
        choices: [
          { name: "Express", value: "express", checked: true },
          { name: "Firebase", value: "firebase", checked: true },
          {
            name: "Firebase - Firestore",
            value: "firebase-firestore",
            checked: true
          },
          {
            name: "Firebase - Authentication",
            value: "firebase-authentication",
            checked: true
          }
        ]
      }
    ],
    filterNext: (val, answerResult) => {
      return answerResult.value.indexOf(val.if) > -1;
    }
  }
];
