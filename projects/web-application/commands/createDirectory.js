const M = {
  chalk: require("chalk"),
  fs: require("fs"),
  path: require("path")
};

const MODULE_NAME = M.path.basename(__filename);

module.exports = (value, previousResult) => {
  let dir = value;
  let exists = M.fs.existsSync(dir);
  if (!exists) {
    M.fs.mkdirSync(dir, { recursive: true });
    console.log(M.chalk.green(`${MODULE_NAME}> Directory ${dir} created`));
  } else {
    console.log(
      M.chalk.gray(`${MODULE_NAME}> Directory ${dir} already exists!`)
    );
  }
  return dir;
};
