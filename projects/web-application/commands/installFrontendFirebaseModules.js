const M = {
  chalk: require("chalk"),
  path: require("path"),
  exec: require("child_process").execSync,
  fs: require("fs")
};

const MODULE_NAME = M.path.basename(__filename);

module.exports = (values, previousResult) => {
  console.log(
    M.chalk.yellow(`${MODULE_NAME}> should install: ${values.join(",")}`)
  );
  const clientDir = previousResult.createFrontendDirectory.result;
  console.log(M.chalk.yellow("  > Client directory", clientDir));

  if (values.includes("firebase-firestore")) {
    console.log(M.chalk.yellow("  > TODO: install Firebase Firestore"));
  }
  if (values.includes("firebase-authentication")) {
    console.log(M.chalk.yellow("  > TODO: install Firebase Authentication"));
  }

  return values;
};
