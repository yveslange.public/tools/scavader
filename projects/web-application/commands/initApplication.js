const M = {
  chalk: require("chalk"),
  path: require("path"),
  fs: require("fs"),
  exec: require("child_process").execSync
};

const MODULE_NAME = M.path.basename(__filename);

module.exports = (value, previousResult) => {
  console.log(`${MODULE_NAME}> Init application: ${value}`);
  const MODULES = ["chalk"]
  const MODULES_MISSING = []
  MODULES.forEach( (module) => {
    try {
      require(module);
    } catch(e) {
      MODULES_MISSING.push(module);
    }
  });

  if(MODULES_MISSING.length > 0) {
    console.log(`${MODULE_NAME}> Installing missing dependencies ${MODULES_MISSING.join(" ")}`)
    M.exec(`npm install --save-dev ${MODULES_MISSING.join(" ")} > /dev/null 2>&1`)
  }
  return "";
};
