const M = {
  chalk: require("chalk"),
  path: require("path"),
  exec: require("child_process").execSync,
  fs: require("fs")
};

const MODULE_NAME = M.path.basename(__filename);

module.exports = (values, previousResult) => {
  console.log(
    M.chalk.yellow(`${MODULE_NAME}> should install: ${values.join(",")}`)
  );
  const clientDir = previousResult.createFrontendDirectory.result;
  console.log(M.chalk.yellow("  > Client directory", clientDir));

  if (values.includes("pwa-litelement")) {
    console.log(M.chalk.gray("  > installing PWA-LITEMENT"));
    try {
      M.exec(
        `git clone --depth 1 -b template-no-redux --single-branch https://github.com/Polymer/pwa-starter-kit ${clientDir} >/dev/null 2>&1`
      );
      M.exec(`rm -rf ${clientDir}.git ${clientDir}.github ${clientDir}server`);
      console.log(M.chalk.gray("  > installing dependencies of PWA-LITELEMENT"));
      M.exec(`npm install --production > /dev/null 2>&1`, { cwd: clientDir });
      console.log(M.chalk.green("  ✓ installing PWA-LITEMENT"));
    } catch (e) {
      console.log(
        M.chalk.red("  ✘ could not install PWA-LITELEMENT (already done?)")
      );
      console.error(e);
    }
  }
  if (values.includes("firebase")) {
    console.log(M.chalk.yellow("  > TODO: install Firebase"));
  }

  return values;
};
