const M = {
  chalk: require("chalk"),
  path: require("path")
};

const MODULE_NAME = M.path.basename(__filename);

module.exports = (values, previousResult) => {
  console.log(
    M.chalk.yellow(`${MODULE_NAME}> should install: ${values.join(", ")}`)
  );
  if (values.includes("express")) {
    console.log(M.chalk.yellow("  > TODO: install Express"));
  }
  if (values.includes("firebase")) {
    console.log(M.chalk.yellow("  > TODO: install Firebase"));
  }
  if (values.includes("firebase-firestore")) {
    console.log(M.chalk.yellow("  > TODO: install Firebase Firestore"));
  }
  if (values.includes("firebase-authentication")) {
    console.log(M.chalk.yellow("  > TODO: install Firebase Authentication"));
  }
  return values;
};
