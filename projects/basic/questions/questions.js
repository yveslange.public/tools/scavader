module.exports = [
  {
    message: `What folder name do you want to create ?`,
    name: "createFrondendDirectory",
    command: "createDirectory",
    type: "input",
    default: "exampleOfFolderName/"
  }
];
