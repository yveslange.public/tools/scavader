const M = {
  fs: require("fs"),
  path: require("path")
};

// Module name to use
const MODULE_NAME = M.path.basename(__filename);

module.exports = (value, previousResult) => {
  let exists = M.fs.existsSync(value);
  if (!exists) {
    M.fs.mkdirSync(value, { recursive: true });
    console.log(`${MODULE_NAME}> Directory ${value} created`);
    return true;
  } else {
    console.log(`${MODULE_NAME}> Directory ${value} already exists!`);
    return false;
  }
};
