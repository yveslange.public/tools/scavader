### Changelog

All notable changes to this project will be documented in this file. Dates are displayed in UTC.

Generated by [`auto-changelog`](https://github.com/CookPete/auto-changelog).

#### [v0.0.8](https://gitlab.com/yveslange.public/tools/scavader/compare/v0.0.4...v0.0.8)

> 28 February 2021

- fix: npm audit fix [`3ef019f`](https://gitlab.com/yveslange.public/tools/scavader/commit/3ef019f750564858635757e9aa60b686300898a8)
- docs: more comments in README.md helping to start [`4d16aa5`](https://gitlab.com/yveslange.public/tools/scavader/commit/4d16aa537e11e590fe71f2ca9dbf54268d8b73be)
- fix: ignore if the command file is not present [`2e71a89`](https://gitlab.com/yveslange.public/tools/scavader/commit/2e71a8907c248ce8315bdd74ff7d4c02ef2ec318)

#### [v0.0.4](https://gitlab.com/yveslange.public/tools/scavader/compare/v0.0.3...v0.0.4)

> 7 October 2019

- chore(auto-changelog): add configuration to package.json (fix urls) [`5c68da8`](https://gitlab.com/yveslange.public/tools/scavader/commit/5c68da8bea98d5fb1236916e1096e405aa8174b1)
- fix(package): invalid token in package.json [`83c4bde`](https://gitlab.com/yveslange.public/tools/scavader/commit/83c4bdeccd0cd75f45179178f0df5162261d4c28)
- docs(package): add homepage url [`237d456`](https://gitlab.com/yveslange.public/tools/scavader/commit/237d456d5470462588cff472b1dbcc2c88a0544c)

#### [v0.0.3](https://gitlab.com/yveslange.public/tools/scavader/compare/v0.0.2...v0.0.3)

> 7 October 2019

- feat(project): add simpleHTTP with browser-sync [`461a27f`](https://gitlab.com/yveslange.public/tools/scavader/commit/461a27f60da838bd6c48a30d93efc9ab02220c6b)
- fix(cli): update cli and readme for the init -p option [`d204703`](https://gitlab.com/yveslange.public/tools/scavader/commit/d2047035dcea6959e9f50b46ea47ad72bb857185)
- fix: remove useless scaffold/ folder [`5eb2d89`](https://gitlab.com/yveslange.public/tools/scavader/commit/5eb2d89a4fe86b704a1d89fa885f2b44d34362af)

#### [v0.0.2](https://gitlab.com/yveslange.public/tools/scavader/compare/v0.0.1...v0.0.2)

> 5 October 2019

- chore(deps): add changelogs + autochange-logs [`392e18e`](https://gitlab.com/yveslange.public/tools/scavader/commit/392e18e7235986d11838f6c45d4d1404b3393444)
- chore: Add CONTRIBUTING [`1ad9c08`](https://gitlab.com/yveslange.public/tools/scavader/commit/1ad9c087bc724c41d68ade653df03f085b297adc)
- docs(readme): add some usecase example [`b5795f8`](https://gitlab.com/yveslange.public/tools/scavader/commit/b5795f8bfa17ac21e90fdb6d26230314155b2bdd)

#### v0.0.1

> 5 October 2019

- docs(jsdoc): generating JSDoc [`2b421be`](https://gitlab.com/yveslange.public/tools/scavader/commit/2b421be4956673edcc423f2ab79585bade8720f5)
- docs: adds index.html redirection [`9b5efb9`](https://gitlab.com/yveslange.public/tools/scavader/commit/9b5efb908e1f8bb552580aa45175b80a64119cf1)
- feat(menu): implements basic menu to scaffold things [`70af330`](https://gitlab.com/yveslange.public/tools/scavader/commit/70af33028128fb0236fdccd73a91070ad80f20b5)
