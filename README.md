# Scavader

![screenshot-demo](https://gitlab.com/yveslange.public/tools/scavader/raw/master/images/screenshot.gif)

Some true magic about scaffolding a web [backend/front]-end application !

- Create your own wizard to scaffold your project.
- Use one of the built-in scaffold to generate your project (WIP)

What is it for ? Let's say that every time you start a new project on the same stack, 
you are doing the same things over and over again. Here is a usecase example:

- Create a repository and cloning it
- Generating the NPM package.json file with `$ npm init`
- Removing some directories
- Editing the titles
- Removing some useless file
- Removing some useless portion of codes
- Installing other dependencies
- Moving file around
- Creating a new README.md and DOCS.md
- ...

Why don't you write down those steps in a JSON file once and use it in
**Scavader** each time you want to initialize a new projet ? You can configure
this tool to ask you some custom questions and execute some custom commands. 


## Features
- Nice command line interface
- Easy wizard creation using JSON 
- Built-in scaffolds 

## Installation

`npm install -g @scavader/scavader`

or

`npm install --save scavader` (and adapt the commands futher with `./node_modules/.bin/scavader ...`)

## Getting started

### Create a new scaffold

Initialize the scaffolding structure: `scavader init`. This will generate a
scaffold wizard example where you can start working in.

- Edit the `./scaffold/questions` to customize your scaffold wizard.
- Edit the `./scaffold/commands` to customize what your scaffold should do.

### Run your scaffold
- Run your new scaffold wizard using: `scavader run`

## Command Line Interface (CLI)

Run *scavader* without any arguments to see the help menu.  By default,
scavader will initialize and run using the `./scaffold` folder name.

- Scaffold from built-in project:
  ```bash
  $ scavader init [-p projectName] [-d directoryName]
  $ scavader run [-d directoryName]
  ```

- Scaffold a specific built-in project (here the `web-application` project) and output
  to a specific directory:
  ```bash
  $ scavader init -p web-application -d myscavader 
  $ scavader run -d myscavader
  ```

- Create a basic scaffold and run the wizard
  ```bash
  $ scavader init -d myscavader 
  $ scavader run -d myscavader
  ```

- Run scaffold wizard using another set of *questions* (you can create other
  question(s) file(s) in the `./scaffold/questions` directory and use the `-q`
  option to specify which one to use):
  ```bash
  $ scavader run -d myscavader -q secondary_questions
  ```


## Example of usage in a script

```javascript
Scavader = require("scavader")

// Question(s) file to use
const questionsFile = "questions";

// Example of scavader using the `./scaffold.example` folder and run the
// questions located in `./scaffold.example/questions/questions.js`
let scavader = new M.Scavader("./scaffold/", "Scavader");
scavader.run(questionsFile).then(result => {
  console.log(result);
});
```

## Built-in projects (WIP)
- `basic`: a basic example to demonstrate how scaffold are made.
- `simple-http`: A simple and basic HTTP server with a single index.html file.
- `web-application`: this will run a wizard asking you things
   about the server and client framework you want to use.
  - As of today: only lit-element (client side) and NodeJS +
    firebase (server side) are supported
- More soon. This is not my priority right now. Therefore, some help would be appreciated

## Create your own scaffold

Take a look at the `./projects/web-application/questions` and
`./projects/web-application/commands` example that stands in this repository. 

The question file is nothing more than a list of questions that have a specific
format. Check the built in projects to have more insights. You should be aware
that the `type` and `choices` fields are the same as in the
[Inquirer](https://www.npmjs.com/package/inquirer) package. You can have all of
them: `input, number, confirm, list, rawlist, expand, checkbox, password,
editor` (as of 2020-03-06).

## Demonstration and documentation

Go to the Scavader package directory (`cd node_modules/scavader`) or
download a clone of the project from github.com and run `npm install`.

- Run the demonstration: `npm run demo`
- Launch the offline documentation: `npm run doc`

## History
Scavader means Scaffolder-Invader, this tools was build to quickly scaffold my
own projects. 

## Author and Contributor
Yves Lange
