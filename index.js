#!/usr/bin/env node

// Imports and check all dependency modules
require("./libs/_check_imports");
const M = {
  chalk: require("chalk"),
  path: require("path"),
  Menu: require("./libs/menu"),
  Scaffold: require("./libs/scaffold")
};

const SCAFFOLD_DEFAULT_DIR = "./scaffold";

/**
 * The magic of scaffolding
 *
 * ### Create the structure
 *
 * Run: `scavader init [-d directoryName]`
 *
 * By default the directoryName will be `./scaffold`
 *
 * ### Example of usage
 *
 * You can either use the CLI to launch a scaffold or you can use a script.
 *
 * #### CLI
 *
 * ```
 * scavader init [-d scaffold]
 * scavader run [-d scaffold] [-q questions]
 * ```
 *
 * #### Script
 * ```
 * Scavader = require("scavader")
 *
 * let scavader = new Scavader("./scaffold.example/", "Scavader");
 *
 * scavader.run("questions").then(result => {
 *   console.log(result);
 * });
 * ```
 *
 * ### Configuration object
 *
 * | Field name | Description |
 * |------------|-------------|
 * | scaffoldQuestionDir | Overrides the subdirectory path of questions (default: 'questions/') |
 * | scaffoldCommandsDir | Overrides the subdirectory path of questions (default: 'commands/') |
 *
 * @see module:menu
 * @see module:scaffold
 */
class Scavader {
  /**
   * Create a Scavader instance.
   *
   * @param {string} scaffoldDir The directory of the scaffold commands and questions (default: 'scaffold')
   * @param {string} title The title to be displayed when running the menu of questions.
   * @param {object} config A configuration object that let you overrides some internal values.
   */
  constructor(scaffoldDir = SCAFFOLD_DEFAULT_DIR, title = "Scavader", config) {
    this.scaffoldDir = scaffoldDir;
    this.config = Object.assign(
      {
        scaffoldQuestionsDir: "questions/", // Scaffold questions subdirectory path to be override
        scaffoldCommandsDir: "commands/" // Scaffold commands subdirectory path to be overrid
      },
      config
    );

    this.Menu = new M.Menu(title);

    this.Scaffold = new M.Scaffold(
      `${this.scaffoldDir}/${this.config.scaffoldCommandsDir}/`
    );
  }

  /**
   * Run a sequence of questions and returns a promise that will resolves
   * returning the answers.
   *
   * @param {array} questionModule An array of question to be asked
   * @return {Promise} The promise that resolves the answers
   */
  run(questionsFile) {
    return new Promise((resolve, reject) => {
      let questionPath =
        process.cwd() +
        "/" +
        M.path.normalize(
          `${this.scaffoldDir}/${this.config.scaffoldQuestionsDir}/${questionsFile}`
        );
      try {
        let questionsFile = require(questionPath);
        this.Menu.run(questionsFile).then(resultsMenu => {
          let scaffolds = this._transformQuestionsResultToScaffold(resultsMenu);
          this.Scaffold.run(scaffolds).then(resultScaffold => {
            resolve(resultScaffold);
          });
        });
      } catch (e) {
        console.error(
          M.chalk.red(
            `ERROR: Scavager could not load or execute the questions file (path=${questionPath})`
          )
        );
        reject(e);
      }
    });
  }

  /**
   * Transforms the result of the menu to be used by the scaffold command
   * exectuer.
   */
  _transformQuestionsResultToScaffold(resultsMenu) {
    return resultsMenu.map(result => {
      return {
        name: result.question.name,
        command: result.question.command,
        value: result.value
      };
    });
  }
}

if (require.main === module) {
  const CLI = require("./libs/cli");
  CLI(Scavader, SCAFFOLD_DEFAULT_DIR);
} else {
  module.exports = Scavader;
}
